package de.xeno.HomeControl;

import com.google.common.eventbus.Subscribe;

import de.xeno.HomeControl.core.EventBus.NewTry.EventHandler;
import de.xeno.HomeControl.core.model.EventType;
import de.xeno.HomeControl.core.model.IOEvent;


public class Reciver {
 
	private static int eventsHandled;
	private EventHandler eventhandler = new EventHandler();

	@Subscribe
    public void EventBus(Object event) {
		eventhandler.setEventType(EventType.IoEvent);
    		Object object = eventhandler.IncommingEvent(event);
    		
    		if (object instanceof IOEvent) {
    			IOEvent eventintance = (IOEvent) object;
    			
    			System.out.println(eventintance.getEventmessage());
    			
    		}
    		System.out.println("New event on: " + this.toString());
    		
    }

	public static int getEventsHandled() {
		return eventsHandled;
	}

	public void setEventsHandled(int eventsHandled) {
		Reciver.eventsHandled = eventsHandled;
	}
	
}
