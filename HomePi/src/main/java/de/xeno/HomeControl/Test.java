package de.xeno.HomeControl;

import java.beans.ConstructorProperties;
import java.lang.reflect.Constructor;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

public class Test {
	static EventBus eventBus;
	
    public Test(EventBus BusHandle) { 
        this.eventBus = BusHandle;
    } 
	
    @Subscribe
	public void givenStringEvent_whenEventHandled_thenSuccess() {
	    eventBus.post("String Event");
	    System.out.println("Post Event from external class ( Test.java)");

}
}
