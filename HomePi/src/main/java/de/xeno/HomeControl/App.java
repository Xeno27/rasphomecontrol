package de.xeno.HomeControl;


import java.io.IOException;
import java.lang.annotation.ElementType;

import com.google.common.eventbus.EventBus;

import de.xeno.HomeControl.Reciver;
import de.xeno.HomeControl.core.EventBus.NewTry.EventHandler;
import de.xeno.HomeControl.core.IOControl.HS100;
import de.xeno.HomeControl.core.IOControl.RaspEventIoController;
import de.xeno.HomeControl.core.model.EventType;
import de.xeno.HomeControl.core.model.IOEvent;

/**
 * Hello world!
 *
 */
public class App 
{
	static EventBus eventBus = new EventBus("IOEventBus");
	static Reciver listener = new Reciver();
	static Test test = new Test(eventBus);
	
	static EventHandler eventhandler = new EventHandler();



	static IOEvent ioevent = new IOEvent();
	
    public static void main( String[] args )
    
    {
    	
    		IOEvent event = new IOEvent();
    		event.setEventtype(EventType.IoEvent);
    		event.setEventmessage("this message musst print !");
    		
    		ioevent.setEventmessage("Event Message");
    		//RaspEventIoController RspEvntContrl = new RaspEventIoController(eventBus);
    		eventBus.register(listener);
    		eventBus.register(eventhandler);
    		//eventBus.register(RspEvntContrl);
    		
    		eventhandler.setEventBus(eventBus);
    		
    		eventBus.post(event);
    		
    		System.out.println(eventBus.identifier());
    	
    		
    		for (int i = 0; i < 10; i++) {
    			givenStringEvent_whenEventHandled_thenSuccess();
			}
    		
    		eventBus.unregister(listener);
    	
    		for (int i = 0; i < 10; i++) {
    			givenStringEvent_whenEventHandled_thenSuccess();
			}
    		
    		System.out.println("Now is:" + listener.getEventsHandled());
    		
    		eventhandler.PostEvent(ioevent);
    		
    		
    		
    		
    }
    
	public static void givenStringEvent_whenEventHandled_thenSuccess() {
		eventhandler.PostEvent("Event");

}

}
