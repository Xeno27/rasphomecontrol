package de.xeno.HomeControl.core.model.actions;

import java.util.Date;

public class GetValue extends Action {
	
	private Float Fvalue;
	private int Ivalue;
	private Date Timestamp;
	
	
	public Float getFvalue() {
		return Fvalue;
	}
	public void setFvalue(Float fvalue) {
		Fvalue = fvalue;
	}
	public int getIvalue() {
		return Ivalue;
	}
	public void setIvalue(int ivalue) {
		Ivalue = ivalue;
	}
	public Date getTimestamp() {
		return Timestamp;
	}
	public void setTimestamp(Date timestamp) {
		Timestamp = timestamp;
	}
	
	
	

}
