package de.xeno.HomeControl.core.model.IOTypes;

import java.util.EnumSet;

import de.xeno.HomeControl.core.model.Location;

public class HS100_IO {

	private String IP;
    private int Port;
	private Location location;
	
	
	public String getIP() {
		return IP;
	}
  
	public void setIP(String value) {
		this.IP = value;
	}
  
	public int getPort() {
		return this.Port;
	}
  
	public void setPort(int value) {
		this.Port = value;
	}
  
	public Location getLocation() {
		return location;
	}
  
	public void setLocation(Location location) {
		this.location = location;
	}

}