package de.xeno.HomeControl.core.Scenario;

import java.util.Date;

import de.xeno.HomeControl.core.model.actions.Action;

public class DateTimeBased {
	
	private Date StartTime;
	private Action StartAction;
	
	public Date getStartTime() {
		return StartTime;
	}
	public void setStartTime(Date startTime) {
		StartTime = startTime;
	}
	public Action getStartAction() {
		return StartAction;
	}
	public void setStartAction(Action startAction) {
		StartAction = startAction;
	}
	

}
