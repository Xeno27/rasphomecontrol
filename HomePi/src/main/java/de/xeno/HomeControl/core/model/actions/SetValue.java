package de.xeno.HomeControl.core.model.actions;

public class SetValue extends Action {
	
	private Float Fvalue;
	private int Ivalue;
	private boolean Bvalue;
	
	public Float getFvalue() {
		return Fvalue;
	}
	public void setFvalue(Float fvalue) {
		Fvalue = fvalue;
	}
	public int getIvalue() {
		return Ivalue;
	}
	public void setIvalue(int ivalue) {
		Ivalue = ivalue;
	}
	public boolean isBvalue() {
		return Bvalue;
	}
	public void setBvalue(boolean bvalue) {
		Bvalue = bvalue;
	}

}
