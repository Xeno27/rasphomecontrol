package de.xeno.HomeControl.core.model;

import de.xeno.HomeControl.core.model.actions.Action;

public class IOEvent {
	
	private Action action; // Internal Action Object for 
	private Input additionalInput; 
	private Output additinalOutput;
	private String eventmessage;
	private EventType eventtype;
	
	public EventType getEventtype() {
		return eventtype;
	}
	public void setEventtype(EventType eventtype) {
		this.eventtype = eventtype;
	}
	public Action getAction() {
		return action;
	}
	public void setAction(Action action) {
		this.action = action;
	}
	public Input getAdditionalInput() {
		return additionalInput;
	}
	public void setAdditionalInput(Input additionalInput) {
		this.additionalInput = additionalInput;
	}
	public Output getAdditinalOutput() {
		return additinalOutput;
	}
	public void setAdditinalOutput(Output additinalOutput) {
		this.additinalOutput = additinalOutput;
	}
	
	public String getEventmessage() {
		return eventmessage;
	}
	public void setEventmessage(String eventmessage) {
		this.eventmessage = eventmessage;
	}
	
}
