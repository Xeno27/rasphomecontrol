package de.xeno.HomeControl.core.IOControl;

import java.util.List;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

import de.xeno.HomeControl.core.model.Input;
import de.xeno.HomeControl.core.model.Location;
import de.xeno.HomeControl.core.model.Output;
import de.xeno.HomeControl.core.model.Value;
import de.xeno.HomeControl.core.model.IOTypes.HS100_IO;
import de.xeno.HomeControl.core.model.IOTypes.Raspberry_IO;
import de.xeno.HomeControl.core.model.actions.Action;
import de.xeno.HomeControl.core.model.actions.GetValue;
import de.xeno.HomeControl.core.model.actions.SetValue;

public class IoApapter {

	private static final String Loaction = null;

	// create gpio controller instance
	final GpioController gpio = GpioFactory.getInstance();

	private List<Action> ActionsTodo;
	private List<Action> ActionDone;

	public void addnewAction(Action action) {
		ActionsTodo.add(action);
		System.out.println("Add new action to IoAdapter : " + action.getFunction() + " " + action.getName());
		
		StartWorking();
	}

	public List<Action> getActionstodo() {
		return ActionsTodo;
	}

	public List<Action> getActionDone() {
		return ActionDone;
	}

	public void StartWorking() {

		System.out.println("Start Working on action ..");

		// For Each Element in Todo List
		for (Action action : ActionsTodo) {

			System.out.println("Working on new action ..");
			System.out.println("name:" + action.getName());

			// If Action is Set Value
			if (action instanceof SetValue) {

				System.out.println("New action setvalue...");
          
				if (action.getSource() instanceof HS100_IO) {
					System.out.println("New HS100 Set Value Opject");}
				
				else if (action.getSource() instanceof Raspberry_IO) {
					System.out.println("New Raspberry_IO Set Value Opject");
					Output output = (Output) action.getTarget();
				    Raspberry_IO raspipin = (Raspberry_IO) output.getType();
				    Value value = output.getValue();
				    
				    if(output.getLocation() == Location.OnBoard) {
					      GpioPinDigitalOutput Output = gpio.provisionDigitalOutputPin(raspipin.getpin(), output.getName(),PinState.LOW);
					     
					      if (value.getDvalue() == 1.0) {
						      System.out.println("Control Output to 1");
						      Output.high();
					    
					      } else if (value.getDvalue() == 0.0) {
						        System.out.println("Control Output to 0");
						        Output.low();
					          
					      } else {
						            throw new UnsupportedOperationException("Not Possible to Control" + value.getDvalue());
					              }
				    }
        }
            
        			}
			// If Action in Get Value
			else if (action instanceof GetValue) {

				System.out.println("New action getvalue...");

				Input input = (Input) action.getTarget();
				Raspberry_IO raspipin = (Raspberry_IO) input.getType();
				Value value = input.getValue();

				GpioPinDigitalInput Input = gpio.provisionDigitalInputPin(raspipin.getpin(), input.getName(),
						raspipin.getPinresitance());

				if (Input.isHigh() == true) {
					value.setDvalue(1.0);
				} else if (Input.isLow() == true) {
					value.setDvalue(0.0);
				} else {
					throw new UnsupportedOperationException("Not good Input state !! .. ");
				}

			}
			// If there is a unsupported action !
			else {

				throw new UnsupportedOperationException(
						" no function to this action possible ! check code IoAdapter ..");
			}

			System.out.println("Set Action" + action.getFunction() + "to Done ...");
			ActionDone.add(action);
			ActionsTodo.remove(action);

		}
		
		System.out.println("All actions are done !");

	}

}
