package de.xeno.HomeControl.core.IOControl;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import de.xeno.HomeControl.core.EventBus.NewTry.EventHandler;
import de.xeno.HomeControl.core.model.EventType;
import de.xeno.HomeControl.core.model.IOEvent;
import de.xeno.HomeControl.core.model.actions.Action;

public class RaspEventIoController {
	
	EventHandler eventhandler = new EventHandler();
	private IoApapter ioadapter = new IoApapter();
	EventBus eventBus;
	
	// Constructor
	public RaspEventIoController(EventBus eventbus) {
		this.eventBus = eventbus;
		//eventBus.register(this);
		eventhandler.setEventBus(eventbus);
	}
	
	public EventBus getEventBus() {
		return eventBus;
	}

	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	@Subscribe
    public void EventBus(Object event) {
		
		eventhandler.setEventType(EventType.IoEvent);
		
		if(eventhandler.IncommingEvent(event) == EventType.IoEvent) {
			IOEvent ioevent = (IOEvent) event;	
			System.out.println("new IOevent on " + this.toString());
			ioadapter.addnewAction(ioevent.getAction());
			
			//ioadapter.StartWorking();
		}
		
	
    }

	

}
