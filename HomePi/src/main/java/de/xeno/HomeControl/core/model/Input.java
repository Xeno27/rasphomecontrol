package de.xeno.HomeControl.core.model;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Input {
	
	
	private Location location;
	private String name = "";
	private String function = "";
	private Object Type;
	private Value value;

	public Value getValue() {
		return value;
	}
	
	public Value setValue(Value value) {
		
		this.value = value;
		return this.value;
	}
	
	public Location getLocation() {
		return this.location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public Object getType() {
		return Type;
	}
	public void setType(Object type) {
		Type = type;
	}
	

}
