package de.xeno.HomeControl.core.EventBus.NewTry;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import de.xeno.HomeControl.core.model.EventType;
import de.xeno.HomeControl.core.model.IOEvent;

public class EventHandler {
	
	EventBus eventBus ; // Global EventBus Object
	EventType eventtype; // Eventytpe you want to recive 
	
	public void setEventBus(EventBus eventbus) {
		this.eventBus = eventbus;
		System.out.println("Set EventHanlder ! " + this.toString());
	}
	
	/*
	 * Set Eventtype you want to recive 
	 */
	public void setEventType(EventType EventType) {
		this.eventtype = EventType;
	}
	
	/*
	 * Post Event on the Eventbus 
	 */
	public void  PostEvent(Object object) {
		
		if (eventBus == null) {
			throw new UnsupportedOperationException("Eventhandler not set !! " + this.toString());
		}
		else {
			eventBus.post(object);
		}
	}
	
	/*
	 * Recive and Filter Events on Eventtype 
	 */
    @Subscribe
	public Object IncommingEvent(Object event) {
		
		Object Return = null;
		
		if (event instanceof IOEvent) {
			
			IOEvent IncommingEvent;
			IncommingEvent = (IOEvent) event;
			
			if(IncommingEvent.getEventtype() == this.eventtype) {
				Return = IncommingEvent;
				System.out.println("new IOevent on " + this.toString());
			}
			else if (null == this.eventtype) {
				Return = IncommingEvent;
				System.out.println("The eventType was not set !! " + this.toString());
			} {
				
			}
			
		}

		return Return;

		
	}

	
}
