package de.xeno.HomeControl.core.model.IOTypes;

import java.util.EnumSet;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinEdge;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;

import de.xeno.HomeControl.core.model.Location;

public class Raspberry_IO {

	private Pin pin;
	private PinPullResistance pinresitance;
	private Location location;
	
	
	public Pin getpin() {
		return pin;
	}
	public void setRpin(Pin value) {
		this.pin = value;
	}
	public PinPullResistance getPinresitance() {
		return pinresitance;
	}
	public void setPinresitance(PinPullResistance pinresitance) {
		this.pinresitance = pinresitance;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	
	
	

}
