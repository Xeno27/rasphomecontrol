package de.xeno.HomeControl.core.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Value {
	
	private Double Dvalue;
	private String Type;
	private String unit;
	private Date valueTimestamp;
	
	
	public Date getValueTimestamp() {
		return valueTimestamp;
	}
	public void setValueTimestamp(Date valueTimestamp) {
		this.valueTimestamp = valueTimestamp;
	}
	public Double getDvalue() {
		return this.Dvalue;
	}
	public void setDvalue(Double fvalue) {
		
		// When set new value also generate new Timestamp vor valid Data
		SimpleDateFormat s = new SimpleDateFormat("yyyyMMddHHmmssSS");
		Date d = new Date();
		valueTimestamp = d;
		
		this.Dvalue = fvalue;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	
	
}
